#!/bin/sh

set -e

VERSION=$2
FILE=$3

NEWVERSION=${VERSION}+1
NEWFILE=../broccoli-python_${NEWVERSION}.orig.tar.xz

echo "Generating ${NEWFILE} ..."
zcat $FILE \
    | tar --wildcards --delete -f - '*/.git' \
    | xz -c > ${NEWFILE}.t

mv ${NEWFILE}.t ${NEWFILE}
